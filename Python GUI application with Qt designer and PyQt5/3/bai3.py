from PyQt5 import QtWidgets, uic
#nhap vao khoangtrong1 gia tri a an nutan1 -> xuat gia tri a*1.24 ra khoangtrong2


#chuong trinh con chuyen doi gia tri (khoang trong 1) 1->(khoang trong 2) 1*1.24
def Chuyendoi():
    dlg.khoangtrong2.setText(str(float(dlg.khoangtrong1.text())*24000))
    
#phan lien ket den giao dien form 
app = QtWidgets.QApplication([])
dlg= uic.loadUi("bai2.ui")


#khi an nutnhan1 -> kich hoat chuong trinh con Chuyendoi
#dlg.khoangtrong1.setFocus()
dlg.khoangtrong1.setPlaceholderText("vnd")
#dlg.khoangtrong2.setFocus()
dlg.khoangtrong2.setPlaceholderText("$")
dlg.nutan1.clicked.connect(Chuyendoi)

dlg.khoangtrong1.returnPressed.connect(Chuyendoi)#chuyen doi nguoc 2 sang 1 
dlg.khoangtrong2.setReadOnly(True) #khoa khoangtrong2 khong cho nhap so 

#show va chay form
dlg.show()
app.exec()
